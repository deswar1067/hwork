import mysql.connector
from mysql.connector import Error


def create_connection(localhost, root, password):
    my_connection = mysql.connector.connect(
        host = localhost,
        user = root,
        passwd = password
    )
