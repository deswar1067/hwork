class School:
    users = {}

    def adds(self, name, lastname, number_group, academic_performance):
        user = {name + " " + lastname: [number_group, academic_performance]}
        self.users.update(user)
        return self.users

    def get_mark(self):
        import re
        for key, value in self.users.items():
            key = re.split(' ', re.sub('[\[\],]', '', key))

            for mark in value[1].values():
                if mark == 5 or mark == 6:

                    print(key, value[0], value[1])
                    print()

                return {key[0]:[value[0], value[1]]}

    def numbergroup(self):
        numbergroup = input('Введите группу')
        for key, value in self.users.items():
            if numbergroup == value[0]:
                print("Список людей группы {}: {}".format(numbergroup, key))
                return numbergroup, key
            else:
                print('Такой группы не существует!')
                return None

    def avtomat(self):
        for key, value in self.users.items():
            if (sum(value[1].values()) / len(value[1])) >= 7:
                print("На автомат претендует: {}".format(key))


s = School()
# my = s.adds('Dzmitri', 'Gavr', '45', {'Math': 5, 'Fizik': 8, 'Cultura': 4,
#                                       'Biologi': 9, 'RusLang': 7})
# my_1 = s.adds('Lena', 'Gavr', '45', {'Math': 9, 'Fizik': 9, 'Cultura': 8,
#                                      'Biologi': 7, 'RusLang': 7})
# my_2 = s.adds('Gena', 'Gnom', '43', {'Math': 6, 'Fizik': 7, 'Cultura': 9,
#                                      'Biologi': 7, 'RusLang': 6})
s.get_mark()
s.numbergroup()
s.avtomat()
