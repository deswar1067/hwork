"""
Напечатайте номер заказа, дату заказа и количество для каждого заказа, Который продал продавец под номером: 5002
Напечатайте уникальные id продавца(salesman_id). Используйте distinct
Напечатайте по порядку данные о дате заказа, id продавца, номер заказа, количество
Напечатайте заказы между 70001 и 70007(используйте between, and)
"""

# from create_base_mysql import *
# import mysql
import mysql.connector as mysql

try:
    connection = mysql.connect(

        host='localhost',
        user='root',
        passwd='password',
        database='shop_base'

    )
    print('Seccessfully connected...')
    print("#" * 20)

    try:
        cursor = connection.cursor()

        # Create table
        # with db.cursor() as cursor:
        #     create_table_query = "CREATE TABLE `users`(id int AUTO_INCREMENT," \
        #                          " ord_no varchar(32)," \
        #                          " purch_amt varchar(32)," \
        #                          " ord_date varchar(32)," \
        #                          "customer_id varchar(32)," \
        #                          " salesman_id varchar(32), PRIMARY KEY (id));"
        #     cursor.execute(create_table_query)
        #     print("Table created successfully")

        # Insert data
        # with connection.cursor() as cursor:
        #     query = "INSERT INTO 'users' (ord_no, purch_amt, ord_date, customer_id, salesman_id) VALUES " \
        #             "(%s, %s, %s, %s, %s);"
        #     values = [
        #         ("70001", "150.5", '2012-10-05', "3005", "5002"),
        #         ("70009", "270.65", '2012-09-10', "3001", "5005"),
        #         ("70002", "65.26", '2012-10-05', "3002", "5001"),
        #         ("70004", "110.5", '2012-08-17', "3009", "5003"),
        #         ("70007", "948.5", '2012-09-10', "3005", "5002"),
        #         ("70005", "240.6", '2012-07-27', "3007", "5001"),
        #         ("70008", "5760", '2012-09-10', "3002", "5001"),
        #         ("70010", "1983.43", '2012-10-10', "3004", "5006"),
        #         ("70003", "2480.4", '2012-10-10', "3004", "5006"),
        #         ("70012", "250.45", '2012-06-27', "3008", "5002"),
        #         ("70011", "75.29", '2012-08-17', "3003", "5007"),
        #         ("70013", "3045.6", '2012-04-25', "3002", "5001")
        #     ]
        # cursor.executemany(query, values)
        # connection.commit()

        # sort_one
        """Напечатайте номер заказа, дату заказа и количество для каждого заказа, Который продал продавец 
             под номером: 5002"""
        with connection.cursor() as cursor:
            select_5002 = "SELECT ord_no, ord_date, customer_id FROM `users` WHERE salesman_id = 5002 "
            cursor.execute(select_5002)
            print("Данные по продавцу 5002:")
            data = cursor.fetchall()
            for i in data:
                print(i)
            print("#" * 20)

        # sort_two
        """Напечатайте уникальные id продавца(salesman_id). Используйте distinct"""
        with connection.cursor() as cursor:
            select_id = "SELECT DISTINCT salesman_id  FROM `users`"
            cursor.execute(select_id)
            print('Уникальные id  продавцов:')
            print(cursor.fetchall())
            print("#" * 20)

        # sort_tree
        """Напечатайте по порядку данные о дате заказа, id продавца, номер заказа, количество"""
        with connection.cursor() as cursor:
            select_id = "SELECT ord_date, salesman_id, ord_no, customer_id FROM `users` ORDER BY ord_date"
            cursor.execute(select_id)
            print('Сортировка по дате покупок:')
            data = cursor.fetchall()
            for i in data:
                print(i)
            print("#" * 20)

        # sort_four
        """Напечатайте заказы между 70001 и 70007(используйте between, and)"""
        with connection.cursor() as cursor:
            select_id = "SELECT * FROM `users` WHERE ord_no  BETWEEN '70001' AND '70007' "
            cursor.execute(select_id)
            print('Сортировка по заказам между 70001 и 70007:')
            sort = cursor.fetchall()
            for i in sort:
                print(i)
            print("#" * 20)

        # select all data from table
        # with connection.cursor() as cursor:
        #     select_all_rows = "SELECT * FROM `users`"
        #     cursor.execute(select_all_rows)
        #     # cursor.execute("SELECT * FROM `users`")
        #
        #     rows = cursor.fetchall()
        #     for row in rows:
        #         print(row)
        #     print("#" * 20)


    finally:
        connection.close()

except Exception as ex:
    print("Connection refused...")
    print(ex)
