import json

with open('students.json') as file:
    data = json.load(file)
    print(data)


def student(group: str, club: str):
    """Поиск учеников содного класса и секции"""
    print("Список ребят с одного класса и секции")
    for x in data:
        if x["Class"] == group and x["Club"] == club:
            print(f"{x['Name']}", end="\n")


def gender_man():
    """Фильтрация по полу"""
    print("Мужского пола:")
    for man in data:
        if man["Gender"] == "M":
            print(f"{man['Name']}", end="\n")


def gender_woman():
    """Фильтрация по полу"""
    print("Женского пола:")
    for woman in data:
        if woman["Gender"] == "W":
            print(f"{woman['Name']}", end="\n")


def name_student(name):
    """Поиск по имени"""
    print("Поиск по имени:")
    for x in data:
        if x["Name"].startswith(name):
            print(f"{x['Name']}", end="\n")
        elif x["Name"].endswith(name):
            print(f"{x['Name']}", end="\n")


student('1b', 'Football')
gender_man()
gender_woman()
name_student("Miyu")
