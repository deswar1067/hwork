def calculytor():
  number_1 = int(input('Введите 1-е число: '))
  number_2 = int(input('Введите 2-е число: '))
  oper = input('''
Пожалуйста введите математический оператор указанный ниже:
+ для сложения
- для вычитания
* для умножения
/ для деления
''')

  if oper == '+':
   print('{} + {} = '.format(number_1, number_2))
   print(number_1 + number_2)
  elif oper == '-':
   print('{} - {} = '.format(number_1, number_2))
   print(number_1 - number_2)
  elif oper == '*':
   print('{} * {} = '.format(number_1, number_2))
   print(number_1 * number_2)
  elif oper == '/':
   print('{} / {} = '.format(number_1, number_2))
   print(number_1 / number_2)
calculytor()