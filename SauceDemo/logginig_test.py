import logging

# from logging.handlers import RotatingFileHandler
from selenium.webdriver.common.by import By


def test_new(registration):
    driver = registration

    logfile = 'report.log'

    log = logging.getLogger("my_log")
    log.setLevel(logging.INFO)
    FH = logging.FileHandler(logfile)
    basic_formater = logging.Formatter('%(asctime)s : [%(levelname)s] : %(message)s')
    FH.setFormatter(basic_formater)
    log.addHandler(FH)

    one = driver.find_element(By.ID, "item_4_title_link")
    log.info(one.text)
    one_price = driver.find_element(By.CSS_SELECTOR, "#inventory_container > div > div:nth-child(1) > "
                                                        "div.inventory_item_description > div.pricebar > div")
    log.info(one_price.text)
    two = driver.find_element(By.ID, "item_0_title_link")
    log.info(two.text)
    two_price = driver.find_element(By.CSS_SELECTOR, "div.inventory_item:nth-child(2) > div:nth-child(2) >"
                                                        " div:nth-child(2) > div:nth-child(1)")
    log.info(two_price.text)
    three = driver.find_element(By.ID, "item_1_title_link")
    log.info(three.text)
    three_price = driver.find_element(By.XPATH, '//*[@id="inventory_container"]/div/div[3]/div[2]/div[2]/div')
    log.info(three_price.text)
    four = driver.find_element(By.ID, "item_5_title_link")
    log.info(four.text)
    four_price = driver.find_element(By.XPATH, '//*[@id="inventory_container"]/div/div[4]/div[2]/div[2]/div')
    log.info(four_price.text)
    five = driver.find_element(By.ID, "item_2_title_link")
    log.info(five.text)
    five_price = driver.find_element(By.XPATH, '//*[@id="inventory_container"]/div/div[5]/div[2]/div[2]/div')
    log.info(five_price.text)
    six = driver.find_element(By.ID, "item_3_title_link")
    log.info(six.text)
    six_price = driver.find_element(By.XPATH, '//*[@id="inventory_container"]/div/div[6]/div[2]/div[2]/div')
    log.info(six_price.text)
    print("Hellow World")
