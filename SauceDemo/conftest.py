"""
1.Создать новый проект SauceDemo
2.Для следующего приложения: https://www.saucedemo.com/.
3.Необходимо найти все локаторы для функциональных элементов, т.е.иконки, кнопки, поля ввода и т.д.\
    a.По сути вы должны иметьпросто текстовый файл с записями в стиле:
        i.By.id(“some_id”)
        ii.By.xpath(“some_xpath”)
    b.Назвать текстовый файл SauceDemoLocators.txt, разместить в папке src / test / resources(*можно внутри
        теста с помощью driver.find_element())
    c.Нужно найти локаторы для окна Login
    d.Залогиниться и найти все элементы в каталоге
    e. * Вывести в лог название товара и его стоимость, с помощью Logging Python
4.Создать Pull Request и добавить ментора в коллабораторы.
"""
import time

import pytest
from selenium import webdriver
from selenium.webdriver.common.by import By

# import requests


url = "https://www.saucedemo.com/"
log = "standard_user"
password = 'secret_sauce'


@pytest.fixture(scope='function')
def registration():
    chrome_options = webdriver.ChromeOptions()
    chrome_options.add_argument('--no-sandbox')
    chrome_options.add_argument('--headless')
    chrome_options.add_argument('--disable-gpu')
    # webdriver.Chrome(chrome_options=chrome_options)
    driver = webdriver.Chrome(chrome_options=chrome_options)
    driver.get(url)
    driver.maximize_window()
    search = driver.find_element(By.ID, 'user-name')
    search.send_keys(log)
    search_field = driver.find_element(By.ID, 'password')
    search_field.send_keys(password)
    button = driver.find_element(By.XPATH, '//*[@id="login-button"]')
    button.click()
    time.sleep(1)
    yield driver

    time.sleep(1)
    driver.close()

    # search_field.submit()
    # self.driver.quit()
    # data = self.driver.find_element(By.XPATH, '//*[@id="inventory_container"]/div/div[1]/div[2]/div[1]/div')
    # print(data)
