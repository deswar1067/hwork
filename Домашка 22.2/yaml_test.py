import json
from pprint import pprint

import yaml

with open('order.yaml') as f:
    templates = yaml.safe_load(f)

print(f"Номер заказа {templates['invoice']}")
print('Адрес отправки')
pprint(templates['bill-to']['address'])
print(f"Описание посылки: {templates['product']}")
