"""
        i. Найти чекбокс
        ii. Нажать на кнопку
        iii. Дождаться надписи “It’s gone”
        iv. Проверить, что чекбокса нет
        v. Найти инпут
        vi. Проверить, что он disabled
        vii. Нажать на кнопку
        viii. Дождаться надписи “It's enabled!”
        ix. Проверить, что инпут enabled
"""

import pytest
from selenium import webdriver

url = "http://the-internet.herokuapp.com/dynamic_controls"
url_1 = 'http://the-internet.herokuapp.com/iframe'


@pytest.fixture(scope='function')
def registration():
    driver = webdriver.Chrome('/usr/bin/chromedriver')
    driver.get(url)
    driver.maximize_window()

    yield driver

    driver.close()


@pytest.fixture(scope='class')
def frames():
    driver = webdriver.Chrome('/usr/bin/chromedriver')
    driver.get(url_1)
    driver.maximize_window()
    yield driver

    driver.close()
