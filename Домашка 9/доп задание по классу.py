"""
1. Создать программу, в которой реализуется множественное
наследование (минимум 3 уровня).
2. Программа может производить какие-то рассчеты или выводить
какие-то данные о животных, технике и т.д.
4. Код должен содержать документацию класса и методов.

"""

class Bike:
    """Это родительский класс с чего зарождался двухколесный транспорт"""

    def __init__(self, frame, wheel, tire, pedal):
        """В этом методе мы инициализировали основу и чего состоит данный класс"""

        print("Базовый комплект cостоит из:{},{},{},{}".format(frame, wheel, tire, pedal))



class MountainBike(Bike):
    """Это дочерний класс. В его основе заложен родитеьский класс с инициализированными параметрами """

    def __init__(self, frame, wheel, pedal, tire, transmission):
        """В данном методе добавляются новые атррибуты, при это сохраняются родительские"""

        super(MountainBike, self).__init__(frame, wheel, tire, pedal)
        print("Здесь обновили - {} и установили: {}".format(transmission, tire))



class MotoBike(MountainBike):
    """Это дочерний класс Выше описанных"""

    def __init__(self, engine, rame, wheel, pedal, tire, transmission):
        """Здесь сохраняются все выше описываемое + добавляется новая фича которая заложила основу эре Мотоциклов!!!"""

        super(MotoBike, self).__init__(rame, wheel, pedal, tire, transmission)
        self.engine = engine
        print("Добавили мощь:{}".format(engine))



class CruiseBike(MotoBike):
    """Чтож тут понятно что у нас сохраняется основа выше описанного класса, но с одним отличием"""

    def __init__(self, passanger_seat, engine, rame, wheel, pedal, tire, transmission):
        """Мотоциклы полюбили не только байкеры, но их вторые половинки(пассажиры). Одной из причин стало
        удобство передвижения, которая описана в данном методе"""

        super(CruiseBike, self).__init__(engine, rame, wheel, pedal, tire, transmission)
        self.passanger_seat = passanger_seat
        print("Добавили прекрасное {}".format(passanger_seat))



b = Bike('стальная рама', "руль", "колесо","педали")
c = MountainBike('стальная рама', "руль", "колесо", "шипованные колеса", "передачи")
f = MotoBike("двигатель", 'стальная рама', "руль", "колесо", "шипованные колеса", "передачи")
g = CruiseBike("Мягкое пассажирское сиденье", "двигатель", 'стальная рама', "руль", "колесо", "шипованные колеса", "передачи")