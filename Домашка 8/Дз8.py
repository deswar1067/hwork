"""1. Напишите функцию декоратор, которая добавляет 1 к заданному числу"""

def gotTalk(type="shout"):
    # Мы определяем функции прямо здесь
    def shout(i = int(input('Введите число'))):
         return i + 10
    # Затем возвращаем необходимую
    if type == "shout":
        return shout

talk = gotTalk()
print (talk)
print (talk())



"""2.Напишите функцию декоратор, которая переводит полученный текст в верхний регистр"""
def getTalk(type="shout"):
    # Мы определяем функции прямо здесь
    def shout(i = (input('Введите текст'))):
         return i.upper()
    # Затем возвращаем необходимую
    if type == "shout":
        return shout

talk = getTalk()
print (talk)
print (talk())




"""3.Напишите декоратор change, который делает так, что задекорированная функция
 принимает все свои не именованные аргументы в порядке, обратном тому, в котором их передали"""


def change(func):
    def wrapper(*args, **kwargs):

        return func(*args[::-1], **kwargs)
    return  wrapper

@change
def sov(*args, show = False):
    res = (*args, show)
    return res

a = sov('dwf', 'wef', 32424, 234, show=True)
print(a)

"""
def get(n):
    l = []
    for i in range(n):
        if i % 2 == 0:
            l.append(i)
    return l
"""








