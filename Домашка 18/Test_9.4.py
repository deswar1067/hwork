import unittest

from Задание_9_4 import School


class TestSchool(unittest.TestCase):
    # def setUp(self):
        # s = School()
        # s.adds('Dzmitri', 'Gavr', '45', {'Math': 5})



    def test_adds(self):
        s = School()
        self.assertEqual(s.adds('Dzmitri', 'Gavr', '45', {'Math': 5}), {"Dzmitri Gavr": ['45', {'Math': 5}]})


    def test_get_mark(self):
        s = School()
        # s.adds('Dzmitri', 'Gavr', '45', {'Math': 5})
        self.assertEqual(s.get_mark(), {"Dzmitri": ['45', {'Math': 5}]})

    def test_nembergroup(self):
        s = School()
        # s.adds('Dzmitri', 'Gavr', '45', {'Math': 5})
        self.assertEqual(s.numbergroup(), ('45', "Dzmitri Gavr"))



if __name__ == '__main__':
    unittest.main()

