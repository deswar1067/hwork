#! /bin/bash

read -p "Please enter character:" i
case $i in
[[:upper:]]) echo "You entered a capital letter";;
[[:lower:]]) echo "You entered a small letter";;
[0-9]) echo "You entered number";;
?) echo "You entered special symbol";;
*)  echo "You entered more than one character";;

esac
