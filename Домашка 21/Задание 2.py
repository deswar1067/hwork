import time

from selenium import webdriver
from selenium.webdriver.common.by import By


class Test:
    url = "http://demo.guru99.com/test/newtours/register.php"
    first_name = 'Dima'
    last_name = 'Gavr'
    phone = 12345
    word = 'deswar1067@tut.by'
    adress = "Losika"
    city = 'Gomel'
    code = 12345
    country = "Belarus"
    password = 'Lolipop'

    def test_new(self):
        self.driver = webdriver.Chrome('/home/deswar/PycharmProjects/pythonProject5/chromedriver')
        self.driver.get(self.url)
        self.driver.maximize_window()

        search_field = self.driver.find_element(By.NAME, "firstName")
        search_field.send_keys(self.first_name)
        search_field = self.driver.find_element(By.NAME, "lastName")
        search_field.send_keys(self.last_name)
        search_field = self.driver.find_element(By.NAME, "phone")
        search_field.send_keys(self.phone)
        search_field = self.driver.find_element(By.NAME, "userName")
        search_field.send_keys(self.word)
        search_field = self.driver.find_element(By.NAME, "address1")
        search_field.send_keys(self.adress)
        search_field = self.driver.find_element(By.NAME, "city")
        search_field.send_keys(self.city)
        search_field = self.driver.find_element(By.NAME, "state")
        search_field.send_keys(self.city)
        search_field = self.driver.find_element(By.NAME, "postalCode")
        search_field.send_keys(self.code)
        search_field = self.driver.find_element(By.NAME, "country")
        search_field.send_keys(self.country)
        search_field = self.driver.find_element(By.NAME, "email")
        search_field.send_keys(self.word)

        search_field = self.driver.find_element(By.NAME, "password")
        search_field.send_keys(self.password)
        search_field = self.driver.find_element(By.NAME, "confirmPassword")
        search_field.send_keys(self.password)
        button = self.driver.find_element(By.CSS_SELECTOR, "[name='submit']")
        button.click()

        if self.driver.find_element(By.XPATH, "//*[contains(text(), 'Your user name is deswar1067@tut.by')]"):
            print(True, self.word)
        if self.driver.find_element(By.XPATH, "//*['Dear Dima Gavr']"):
            print(True, "Dima Gavr")

        time.sleep(2)

        self.driver.quit()
