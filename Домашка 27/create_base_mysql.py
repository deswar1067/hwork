import mysql.connector
# import mysql.connector as mysql

from mysql.connector import Error


def create_connection(host_name, user_name, user_password):
    my_connection = None
    try:
        my_connection = mysql.connector.connect(
            host=host_name,
            user=user_name,
            passwd=user_password

        )
        print("Connection to MySQL DB seccessful")
    except Error as e:
        print(f'Connection error "{e}" occured')

    return my_connection


def create_database(connection, query):
    cursor = connection.cursor()
    try:
        cursor.execute(query)
        print("Database create seccessfully")
    except Error as e:
        print(f"The error '{e}' occurred")


connection = create_connection('localhost', 'root', 'password')
create_database_query = 'CREATE DATABASE shop_base'
create_database(connection, create_database_query)
