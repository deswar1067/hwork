"""
def calculytor():
    number_1 = int(input('Введите 1-е число: '))
    number_2 = int(input('Введите 2-е число: '))
    oper = input('''
Пожалуйста введите математический оператор указанный ниже:
+ для сложения
- для вычитания
* для умножения
/ для деления
''')

    if oper == '+':
        print('{} + {} = '.format(number_1, number_2))
        print(number_1 + number_2)
    elif oper == '-':
        print('{} - {} = '.format(number_1, number_2))
        print(number_1 - number_2)
    elif oper == '*':
        print('{} * {} = '.format(number_1, number_2))
        print(number_1 * number_2)
    elif oper == '/':
        print('{} / {} = '.format(number_1, number_2))
        print(number_1 / number_2)


calculytor()
"""


class Calculytor(Exception):
    def __init__(self, number_1=float, number_2=float):
        self.a = number_1
        self.b = number_2


    def sum(self):
        try:
            c = self.a + self.b
            print(f'{self.a} + {self.b} =', c)
        except TypeError:
            print('Введены неккоректные данные')

    def subtraction(self):
        try:
            c = self.a - self.b
            print(f'{self.a} - {self.b} =', c)
        except TypeError:
            print('Введены неккоректные данные')

    def multiplication(self):
        try:
            c = self.a * self.b
            print(f'{self.a} * {self.b} =', round(c, 1))
        except TypeError:
            print('Введены неккоректные данные')

    def division(self):
        try:
            c = self.a / self.b
            print(f'{self.a} : {self.b} =', round(c, 1))
        except ZeroDivisionError:
            print('Деление на ноль запрещено')
        except TypeError:
            print('Введены неккоректные данные')


class Method(Calculytor):
    def __init__(self, number_1, number_2):
        super(Method, self).__init__(number_1, number_2)

# a = Method(12, 0)
# a.sum()
# a.subtraction()
# a.multiplication()
# a.division()
