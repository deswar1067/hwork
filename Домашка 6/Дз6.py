"""1. Напишите генератор который принимает
список numbers = [34.6, -203.4, 44.9, 68.3, -12.2, 44.6, 12.7]
и возвращает новый список только с положительными числами"""

numbers = [34.6, -203.4, 44.9, 68.3, -12.2, 44.6, 12.7]
numbers = [i for i in numbers if i > 0]
print(numbers)



"""2. Создайте список группы в виде словаря, т.е. каждый член группы должен иметь порядковый номер. 
Используя генератор списков, создайе список дежурств на месяц."""

spisok = ['Антипов И.С.', 'Козляковский М.А.', 'Кухарская Т.В.', 'Козлов М.С.', 'Внук И.А.',
          'Калинина Н.В.', 'Дектярев С.В.', 'Клапоть Е.А.', 'Ржеуцкий В.А.', 'Хадускина С.И.', 'Гаврутиков Д.В.']
number = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11']
spisok_number = dict(zip(number, spisok))

import calendar
a = calendar.monthcalendar(2021, 10)

dict_spisok_number = list(dict.items(spisok_number))

L1 = [a[0], dict_spisok_number[0],dict_spisok_number[1], dict_spisok_number[2]]
L2 = [a[1],dict_spisok_number[3], dict_spisok_number[4], dict_spisok_number[5]]
L3 = [a[2], dict_spisok_number[6], dict_spisok_number[7], dict_spisok_number[8]]
L4 = [a[3], dict_spisok_number[9], dict_spisok_number[10]]
L5 = [a[4], dict_spisok_number[0],dict_spisok_number[1], dict_spisok_number[2]]
print('На первой недели месяца дежурят', L1)
print('На второй недели месяца дежурят', L2)
print('На третьей недели месяца дежурят', L3)
print('На четвертой недели месяца дежурят', L4)
print('На пятой недели дежурят', L5)


"""for i, x in spisok_number.items():
    s = str(i) + "." + str(x)
    print(s)
    print(type(s))"""


'''3.Необходимо составить список чисел которые указывают на длину слов в строке:
 sentence = " the quick brown fox jumps over the lazy dog", но только если слово не "the"'''

sentence = "the quick brown fox jumps over the lazy dog"
lis = sentence.split()
lis_1 = [len(i) for i in lis if i != 'the']
print(lis_1)






