# import pytest as pytest
from selenium.common.exceptions import NoSuchElementException, ElementNotInteractableException
from selenium.webdriver.common.by import By

# from Hwork.SauceDemo.conftest import registration


def test_controls(registration):
    driver = registration
    driver.implicitly_wait(5)
    driver.find_element(By.CSS_SELECTOR, '#checkbox > input[type=checkbox]').click()
    driver.find_element(By.CSS_SELECTOR, '#checkbox-example > button').click()
    driver.find_element(By.XPATH, '//*[@id="message"]')
    try:
        driver.find_element(By.CSS_SELECTOR, '#checkbox > input[type=checkbox]')
    except NoSuchElementException:
        print("Checkbox not found!")
    try:
        driver.find_element(By.CSS_SELECTOR, '#input-example > input[type=text]').send_keys('Hellow')
    except ElementNotInteractableException:
        print('*' * 40)
        print("Input disable!")
    driver.find_element(By.CSS_SELECTOR, '#input-example > button').click()
    driver.find_element(By.CSS_SELECTOR, '#message')
    print('*' * 40)
    print("Input clickable!")

    driver.find_element(By.CSS_SELECTOR, '#input-example > input[type=text]').send_keys('Hellow')
    print('*' * 40)
    print("Input enable!")


def test_frames(frames):
    driver = frames
    # frames = driver.find_element(By.TAG_NAME, 'iframes')
    # print(len(frames))
    driver.switch_to.frame(driver.find_element_by_tag_name("iframe"))

    text = driver.find_element(By.XPATH, "//*[contains(text(), 'Your content goes here.')]")
    print("*" * 10)
    print(text.text)


if __name__ == '__main__':
    print('Running test!!!')
