#! /bin/bash

read -p "Please enter number:" a
if [[ "$a" -gt "15" && "$a" -lt "45" ]]; then
	echo "$a больше 15 но меньше 45"
else echo "$a вне диапазона [15,45]"
fi

if [[ "$a" -lt "-1" || "$a" -eq "45" ]]; then
	echo "$a  меньше -1 или равно 45"
else echo "$a больше -1 и не равно 45"
fi
