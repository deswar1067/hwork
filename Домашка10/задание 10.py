"""
Цветочница
Определить иерархию и создать несколько цветов. Собрать букет (можно использовать аксессуары) с определением его стоимости.
Определить время его увядания по среднему времени жизни всех цветов в букете.
Позволить сортировку цветов в букете на основе различных параметров (свежесть/цвет/длина стебля/стоимость...)
Реализовать поиск цветов в букете по определенным параметрам.
Узнать, есть ли цветок в букете.
"""


class Flowers:
    def __init__(self, name=None, fresh=None, color=None, stem_length=None, price=None):
        self.name = name
        self.fresh = fresh
        self.color = color
        self.stem_length = stem_length
        self.price = price
        self.flowers = {}

    def add(self, name, fresh, color, stem_length, price):
        flowers = {name: [fresh, color, stem_length, price]}
        self.flowers.update(flowers)
        return self.flowers


class Rose(Flowers):
    def __init__(self):
        super().__init__()
        self.rose_name = 'Роза'
        self.rose_fresh = 7
        self.rose_color = 'красный'
        self.rose_stem_length = '70см'
        self.rose_price = 5
        rose = {self.rose_name: [self.rose_fresh, self.rose_color, self.rose_stem_length, self.rose_price]}
        self.flowers.update(rose)


class Tulip(Flowers):
    def __init__(self):
        super().__init__()
        self.tul_name = 'Тюльпан'
        self.tul_fresh = 6
        self.tul_color = 'желтый'
        self.tul_stem_length = '50см'
        self.tul_price = 5
        tulip = {self.tul_name: [self.tul_fresh, self.tul_color, self.tul_stem_length, self.tul_price]}
        self.flowers.update(tulip)


class Chrysanthemum(Flowers):
    def __init__(self):
        super().__init__()
        self.chry_name = 'Хризантема'
        self.chry_fresh = 10
        self.chry_color = 'Белый'
        self.chry_stem_length = '70см'
        self.chry_price = 9
        chry = {self.chry_name: [self.chry_fresh, self.chry_color, self.chry_stem_length, self.chry_price]}
        self.flowers.update(chry)


class Peony(Flowers):
    def __init__(self):
        super().__init__()
        self.peo_name = 'Пиона'
        self.peo_fresh = 12
        self.peo_color = 'желтый'
        self.peo_stem_length = '80см'
        self.peo_price = 13
        peo = {self.peo_name: [self.peo_fresh, self.peo_color, self.peo_stem_length, self.peo_price]}
        self.flowers.update(peo)


class Bouquet(Rose, Peony, Tulip, Chrysanthemum):
    def __init__(self, decoration=None, price=None):
        super(Bouquet, self).__init__()
        if isinstance(decoration, str):
            self.decoration = {decoration: price}
        else:
            self.decoration = {}
        print(self.decoration)

    def wither(self):
        fresh_flowers = [v[0] for v in self.flowers.values()]
        clock = int(sum(fresh_flowers) / len(self.flowers))
        print(f'Букет потеряет свой шарм через {clock} суток')

    def cost(self):
        price = [v[3] for v in self.flowers.values()]
        for key in self.decoration.items():
            if key is not None:
                decor = [v for v in self.decoration.values()]
                cost = sum(price) + sum(decor)
                print(f'Стоимость букета составит {cost} рублей')
        else:
            cost = sum(price)
            print(f'Стоимость букета составит {cost} рублей')

    def display(self):
        print(self.flowers)

    def sorted_fresh(self):
        """Сортировка по свежесте"""
        sorted_fresh = sorted(self.flowers.items(), key=lambda x: x[1])
        print(dict(sorted_fresh))

    def sorted_color(self):
        """Сортировка по цвету"""
        sorted_color = sorted(self.flowers.items(), key=lambda x: x[1][1])
        print(dict(sorted_color))

    def sorted_length(self):
        """Сортировка по длине"""
        sorted_length = sorted(self.flowers.items(), key=lambda x: x[1][2])
        print(dict(sorted_length))

    def sorted_price(self):
        """Сортировка по цене"""
        sorted_price = sorted(self.flowers.items(), key=lambda x: x[1][3])
        print(dict(sorted_price))

    def availability(self, flower):
        for key in self.flowers.keys():
            if key == flower:
               print(True)
            else:
               print (False)


a = Bouquet()
rose_1 = a.add('Роза_1', 3, "желтый", "80см", 9)
rose_2 = a.add('Роза_2', 5, "желтый", "80см", 7)
a.display()
a.wither()
a.cost()
a.sorted_fresh()
a.sorted_color()
a.sorted_price()
a.availability("Тюльпан")
