"""
1.1 Создайте класс book с именем книги, автором, кол-м страниц, флагом, зарезервирована ли книги или нет.
1.2 Создайте класс пользователь который может брать книгу, возвращать, бронировать. Если другой пользователь хочет
 взять зарезервированную книгу(или которую уже кто-то читает - надо ему про это сказать).
"""

class Book:

    count = 0

    def __init__(self, title=None, author=None, page=None, flag=None, booking=False):
        self.title = title
        self.author = author
        self.page = page
        self.flag = flag
        self.booking = booking
        self.books = {}


    def add(self, title=None, author=None, page=None, flag=None, booking=False):
        boo = {title + " " + author: [booking, page, flag]}
        self.books.update(boo)
        Book.count += 1
        return  self.books


    def display(self):
        print("Имя книги: {}\nАвтор книги: {}\nКол-во стр.: {}\nФлаг: {}\nЗарезервирована: {}"
                        .format(self.title, self.author, self.page, self.flag, self.booking))
        print (self.books)


class User(Book):
    def __init__(self):
        super(User, self).__init__()
        #self.name = name
        #self.books = {}

    #def add(self, title=None, author=None, page=None, flag=None, booking=False):
       # boo = {title + " " + author: [booking, page, flag]}
        #self.books.update(boo)
        #return  self.books


    def display(self):
        print (self.books)


    def get_book(self, name, title, author):
        for k, v in self.books.items():
            if str(title + " " + author) == k and v[0] == False:
                self.books.pop(k)
                print("{} взял книгу: {}. Остались книги:".format(name, k))
                return self.books
            if not str(title + " " + author) in self.books.keys():
                print("{} - 'этой книги нет в наличии. Книги в наличии приведены ниже:".format(k))
                return self.books


    def to_book(self, name, title, author):
        for key, value in self.books.items():
            if str(title + " " + author) == key:
                    if value[0] == False:
                        value[0] = True
                        print("{} забронировал книгу: {}!".format(name, key))
                        return self.books
                    else:
                        print("'{}' - этой книги нет в наличиии либо она уже забронирована!".format(key))
                        return  self.books













    

b = Book()
"""
book = Book('Пятый Элемент', 'А.В.Дорс', 145, "Россия"),
book_1 = Book('В погоне за счастьем', "У.Смит", 325, "Америка")
a = b.add('Пятый Элемент', 'А.В.Дорс', 145, "Россия")
d = b.add('В погоне за счастьем', "У.Смит", 325, "Америка")
print('Всего книг: {}'.format(Book.count))
b.display()
"""

s = User()
book_11 = s.add('В погоне за счастьем', "У.Смит", 325, "Америка")
book_12 = s.add('Пятый Элемент', "А.В.Дорс", 145, "Россия")
book_13 = s.add('Тайна 3-й планеты', "А.В.Стоткин", 451, "Россия")
book_14 = s.add('Алхимик', "П.Куэльо", 202, "Испания")
s.display()
print(s.get_book('Bob','В погоне за счастьем', "У.Смит"))
print(s.get_book('Dima','Алхимик', "П.Куэльо"))
print(s.to_book('Bob','Тайна 3-й планеты', "А.В.Стоткин"))
print(s.to_book('Dima','Тайна 3-й планеты', "А.В.Стоткин"))













