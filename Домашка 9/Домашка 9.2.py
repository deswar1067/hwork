"""
Банковский вклад
2.1 Создайте класс инвестиция. Который содержит необходимые поля и методы, например сумма инвестиция и его срок.
2.2 Пользователь делает инвестиция в размере N рублей сроком на R лет под 10% годовых
(инвестиция с возможностью ежемесячной капитализации - это означает, что проценты прибавляются к сумме инвестиции ежемесячно).
2.3 Написать класс Bank, метод deposit принимает аргументы N и R, и возвращает сумму, которая будет на счету пользователя
"""


class Investment:
    def __init__(self, n = float, r = float ):
        """Метод определения инвестиции в у.е. и срока депозита в месяцах"""

        self.n = n
        self.r = r


    def term(self):
        """Срок депозита"""

        return self.r


    def sum(self, n):
        """Метод добавления суммы к основному депозиту"""

        k = self.n + n
        print(k)





class Bank(Investment):

    def __init__(self, name):
        super(Bank, self).__init__()
        self.name = name

    def deposit(self, N, R):
        a = 1
        sum_inv = N
        while a <= R:
            sum_inv = sum_inv +  sum_inv * 0.0083
            a += 1
            #sum_inv = sum_inv
        print('{} - ваша сумма сроком на {} месяца(ев) = {}y.e.'. format(self.name, R, sum_inv))





b = Bank("Вова")
b.deposit(100, 3)
a = Investment(1555, 65)

print(a.term())
a.sum(1356)
