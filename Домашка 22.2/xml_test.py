import xml.etree.cElementTree as ET

tree = ET.parse('lib.xml')
root = tree.getroot()


# def good():
#     for child in root:
#         print(child.tag, child.attrib)
#
#         for appt in child:
#             if not appt.text:
#                 text = "None"
#             else:
#                 text = appt.text
#
#             print(appt.tag + " => " + text)
# good()


def search_author(name):
    """Поиск книги по автору"""
    for elem in root:
        if elem.find('.//author').text.startswith(name):
            print(elem.tag, elem.attrib)
            for appt in elem:
                if not appt.text:
                    text = "None"
                else:
                    text = appt.text

                print(appt.tag + " => " + text)

            # print(elem[1].text)
        if elem.find('.//author').text.endswith(name):
            print(elem.tag, elem.attrib)
            for appt in elem:
                if not appt.text:
                    text = "None"
                else:
                    text = appt.text

                print(appt.tag + " => " + text)
            # print(elem[1].text)


def search_price(price):
    """Поиск по цене"""
    print(f"За сумму в {price}руб  есть такие книги:")
    for elem in root:
        if float(elem[3].text) <= price:
            print(elem.tag, elem.attrib)
            for appt in elem:
                if not appt.text:
                    text = "None"
                else:
                    text = appt.text

                print(appt.tag + " => " + text)
            # print(f"{elem[1].text}")


def search_description(desc):
    """Поиск по описанию"""
    print('С таким описанием есть следующие книги:')
    for elem in root:
        if elem.find('.//description').text.startswith(desc):
            print(elem.tag, elem.attrib)
            for appt in elem:
                if not appt.text:
                    text = "None"
                else:
                    text = appt.text

                print(appt.tag + " => " + text)
            # print(elem[1].text)
        elif elem.find('.//description').text.endswith(desc):
            print(elem.tag, elem.attrib)
            for appt in elem:
                if not appt.text:
                    text = "None"
                else:
                    text = appt.text

                print(appt.tag + " => " + text)
            # print(elem[1].text)


def search_title(text):
    """Поиск по заголовку"""
    print('С таким заголовком есть следующие книги:')
    for elem in root:
        if elem.find('.//title').text.startswith(text):
            print(elem.tag, elem.attrib)
            for appt in elem:
                if not appt.text:
                    text = "None"
                else:
                    text = appt.text

                print(appt.tag + " => " + text)
            # print(elem.tag, elem.attrib)
        elif elem.find('.//title').text.endswith(text):
            print(elem.tag, elem.attrib)
            for appt in elem:
                if not appt.text:
                    text = "None"
                else:
                    text = appt.text

                print(appt.tag + " => " + text)
            # print(elem.tag, elem.attrib)


search_author('Kim')
search_price(40)
search_description("A former architect")
search_title('XML')
