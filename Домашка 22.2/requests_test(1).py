import requests


class Get:
    authors = requests.get("https://fakerestapi.azurewebsites.net/api/v1/Authors")
    catalog = authors.json()

    # print(catalog)

    def list_authors(self):
        """Вывод списков авторов"""
        print("Список авторов:")
        for i in self.catalog:
            print(f"{i['firstName']} {i['lastName']}", end="; ")
        print()

    def id_authors(self, number):
        print("Автор по ID:")
        for i in self.catalog:
            if i['id'] == number:
                print(f"{i['firstName']} {i['lastName']}", end="; ")


def post():
    """Добавляем новую книгу"""
    book = requests.post("https://fakerestapi.azurewebsites.net/api/v1/Books", json={
        "id": 1000,
        "title": "Book 150",
        "description": "TeachMeSkills"
    }).text
    # catalog_book = book.json()
    print(book)

    """Добавляем нового пользователя"""
    user = requests.post("https://fakerestapi.azurewebsites.net/api/v1/Users", json={
        "id": 233,
        "userName": "Dima Gavr",
        "password": "How are You?"
    }).text
    print(user)


def put():
    """Обновляем книгу 10"""
    books = requests.put("https://fakerestapi.azurewebsites.net/api/v1/Books/10", json={
        "id": 10,
        "title": "Book 10",
        "description": "Update book now",
        "pageCount": 1,
        "excerpt": "Hello world!!!",
        "publishDate": "2022-01-04T18:39:19.185Z"
    }).text
    # catalog_book = book.json()
    print(books)


def delete_user():
    """Удаляем пользователя 4"""
    user = requests.delete("https://fakerestapi.azurewebsites.net/api/v1/Users/4", json={"id": 4})
    print(user.status_code)


f = Get()
f.list_authors()
f.id_authors(3)
post()
put()
delete_user()
