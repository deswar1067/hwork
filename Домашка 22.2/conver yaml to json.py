import datetime
import json
from ruamel.yaml import YAML


def DateEncoder(obj):
    if isinstance(obj, (datetime.datetime, datetime.date)):
        return obj.strftime('%Y-%m-%d')


in_file = "order.yaml"
out_file = "order.json"
yaml = YAML(typ="safe")
with open(in_file, "r", encoding="utf-8") as i:
    data = yaml.load(i)

with open(out_file, "w", encoding="utf-8") as o:
    json.dump(data, o, indent=4, default=DateEncoder)


