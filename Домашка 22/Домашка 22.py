import json

import xmltodict

with open('filename.xml') as file:
    doc = xmltodict.parse(file.read())
    dos = json.dumps(doc)
    dos = json.loads(dos)
    data = dos['catalog']['book']
    # print(data)


def id_():
    """Вывод id книг"""
    for i in data:
        print("{}".format(i['@id']), end=" ")
    print()


def count():
    """Подсчет количества книг"""
    print("Количество книг={}:".format(len(data)))
    for i in data:
        print("{}".format(i["title"]))


def sort_price():
    """Сортировка по цене"""
    print(sorted(data, key=lambda x: x['price']))


def sort_date():
    """Сортировка по дате"""
    print(sorted(data, key=lambda x: x['publish_date']))


def book_2000():
    """Поиск книг выпущенных в 2000 году"""
    print('Книги выпущенные в 2000 году:', end=" ")
    for i in data:

        if "2000-01-01" < i['publish_date'] < "2001-01-01":
            print(f"'{i['title']}'({i['publish_date']})", end="; ")
            print()


def genre():
    print("Данные жанра Computer:")
    for i in data:
        if i['genre'] == "Computer":
            print(f"Автор: {i['author']}, Дата: {i['publish_date']}, Цена: {i['price']}, Описание: {i['description']}")


if __name__ == '__main__':
    id_()
    # count()
    # sort_price()
    # sort_date()
    # book_2000()
    genre()
