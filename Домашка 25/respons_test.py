import pytest
import requests


@pytest.fixture()
def login():
    login_data = {
        "email": "eve.holt@reqres.in",
        "password": "pistol"
    }
    response = requests.post(url='https://reqres.in/api/register', json=login_data)
    return response


@pytest.fixture()
def user():
    user_data = {
        "name": "morpheus",
        "job": "leader"
    }
    response = requests.post(url='https://reqres.in/api/users', json=user_data)
    return response


@pytest.fixture()
def delete():
    user_data = {"data": {"id": 2, "email": "janet.weaver@reqres.in", "first_name": "Janet", "last_name": "Weaver",
                          "avatar": "https://reqres.in/img/faces/2-image.jpg"},
                 "support": {"url": "https://reqres.in/#support-heading", "text":
                     "To keep ReqRes free, contributions towards server costs are appreciated!"}}
    response = requests.delete(url='https://reqres.in/api/users/2', json=user_data)
    return response


def test_login(login):
    response = login.json()
    print(response['token'])
    assert response['token'], "QpwL5tke4Pnpja7X4"


def test_user(user):
    response = user
    respon = user.json()
    print(user.json())
    print(response.ok)
    assert response.ok, True
    assert respon['name'], 'morpheus'


def test_delete(delete):
    response = delete
    print(response.ok)
    assert response.ok, True


if __name__ == '__main__':
    print('Hello')
