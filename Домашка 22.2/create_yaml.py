import yaml

doc = """
1: Уголок перфорированный – 1200 м.п.(высотой 3м.)
2: Уголок перфорированный гнутый (наружний угол) – 150 м.п.
3: Серпянка 210 м.п.
4: ГКЛВ – 80 м2
5: Дюбеля 6х60 – 600 шт
6: Клей для ГКЛВ (ilmax, Lux, Knauf) – 280 кг.
7: Краска Alpine EXPERT Fakturfarbe 100 BASE
        • Naturweiss – 2480 м2 (4200кг)
        • Aprico 160 – 240 м2 (408кг)
        • Сameo 55 – 125 м2 (212кг)
        • Barolo 135 – 240 м2 (408кг)
        • Barolo 55  - 133 м2 (226кг)
        • Coalin 105 – 240 м2 (408кг)
        • Verona 110 – 127м2 (215кг)
        • Aprico 170 – 32м2 (55кг)
        • Agave 150 – 34 м2 (58кг)
8: Матовые панели – 17м2
9: Краска акриловая белая(потолок) ТУ РБ 14397461.001-99  - 85кг
10:  Краска акриловая белая (стены)  ТУ РБ 14397461.001-99  - 210кг
11:  Штукатурка цементная  - 2 т 
"""

data = yaml.safe_load(doc)
print(data)

with open('create_yaml.yaml', 'w', ) as file:
    documents = yaml.dump(data, file, indent=4, default_flow_style=False, allow_unicode=True)
